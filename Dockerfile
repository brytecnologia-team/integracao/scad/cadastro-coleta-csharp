FROM microsoft/dotnet

COPY . /app

WORKDIR /app

RUN [ "dotnet", "build" ]

ENTRYPOINT [ "dotnet", "run" ]